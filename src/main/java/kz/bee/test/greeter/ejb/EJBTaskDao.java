package kz.bee.test.greeter.ejb;

import kz.bee.test.greeter.common.TaskStatus;
import kz.bee.test.greeter.dao.TaskDao;
import kz.bee.test.greeter.dao.UserDao;
import kz.bee.test.greeter.entity.Task;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class EJBTaskDao implements TaskDao, Serializable {

    @Inject
    private UserDao userDao;

    @Inject
    private EntityManager entityManager;

    @Override
    public void createTask(Task task) {
        entityManager.persist(task);
    }

    @Override
    public void updateTask(Task task) {
        entityManager.merge(task);
        entityManager.flush();
    }

    @Override
    public void updateTaskStatus(int id, TaskStatus status) {
        Task task = findTaskById(id);
        task.setStatus(status.getValue());
        updateTask(task);
    }

    @Override
    public void removeTask(int id) {
        Task task = findTaskById(id);
        task.setActive(false);
        updateTask(task);
    }


    @Override
    public List<Task> getTasksByUserId(int id) {
        List<Task> tasks = new ArrayList<>();
        try {
            Query query = entityManager.createNamedQuery(Task.GetAllTasks.NAME);
            query.setParameter(Task.GetAllTasks.USER_ID, id);
            tasks = (List<Task>) query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    @Override
    public Task findTaskById(int id) {
        return entityManager.find(Task.class, id);
    }
}
