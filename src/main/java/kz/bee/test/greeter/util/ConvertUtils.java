package kz.bee.test.greeter.util;

import kz.bee.test.greeter.common.TaskStatus;
import kz.bee.test.greeter.common.dto.TaskDTO;
import kz.bee.test.greeter.entity.Task;
import kz.bee.test.greeter.entity.User;

import java.util.ArrayList;
import java.util.List;

public class ConvertUtils {

    public static Task convert(TaskDTO dto, User user) {
        Task task = new Task();
        task.setId(dto.getId());
        task.setUser(user);
        task.setTitle(dto.getTitle());
        task.setDescription(dto.getDescription());
        task.setCreateDate(dto.getCreateDate());
        task.setDueDate(dto.getDueDate());
        task.setActive(dto.isActive());
        task.setStatus(Enum.valueOf(TaskStatus.class, dto.getStatus()).getValue());
        return task;
    }

    public static List<TaskDTO> convert(List<Task> tasks) {
        List<TaskDTO> result = new ArrayList<>();
        for (Task task : tasks) {
            TaskDTO dto = new TaskDTO();
            dto.setId(task.getId());
            dto.setUserId(task.getUser().getId());
            dto.setTitle(task.getTitle());
            dto.setDescription(task.getDescription());
            dto.setCreateDate(task.getCreateDate());
            dto.setDueDate(task.getDueDate());
            dto.setActive(task.isActive());
            dto.setStatus(ValueUtils.valueOf(TaskStatus.class, task.getStatus()).toString());
            dto.setEditable(false);
            result.add(dto);
        }
        return result;
    }

}
