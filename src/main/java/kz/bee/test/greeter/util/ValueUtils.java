/**
 *
 */
package kz.bee.test.greeter.util;

import kz.bee.test.greeter.common.TaskStatus;

public class ValueUtils {

    public static TaskStatus valueOf(Class<TaskStatus> enumType, long value) {
        TaskStatus result = null;
        TaskStatus[] values = enumType.getEnumConstants();
        for (TaskStatus item : values) {

            if (item.getValue() == value) {
                result = item;
                break;
            }
        }
        if (result == null) {
            IllegalArgumentException e =
                    new IllegalArgumentException("No enum const " + enumType.getName() + " with value " + value);
            throw e;
        }
        return result;
    }
}
