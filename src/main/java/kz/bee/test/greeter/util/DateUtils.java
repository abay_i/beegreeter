package kz.bee.test.greeter.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static final String DATE_FORMATE = "dd/mm/yyyy";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMATE);

    public static String formatDate(Date date) {
        return dateFormat.format(date);
    }

    public static Date toDate(String value) throws ParseException {
        return dateFormat.parse(value);
    }

}
