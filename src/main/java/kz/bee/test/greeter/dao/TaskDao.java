package kz.bee.test.greeter.dao;

import kz.bee.test.greeter.common.TaskStatus;
import kz.bee.test.greeter.entity.Task;

import java.util.List;

public interface TaskDao {

    void createTask(Task task);

    void updateTask(Task task);

    void updateTaskStatus(int id, TaskStatus status);

    void removeTask(int id);

    List<Task> getTasksByUserId(int id);

    Task findTaskById(int id);

}
