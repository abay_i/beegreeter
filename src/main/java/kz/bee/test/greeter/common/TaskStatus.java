package kz.bee.test.greeter.common;

public enum TaskStatus {

    ASSIGNED(1),
    DONE(2);

    private int value;

    TaskStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
