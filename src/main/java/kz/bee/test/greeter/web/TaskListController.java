package kz.bee.test.greeter.web;

import kz.bee.test.greeter.common.TaskStatus;
import kz.bee.test.greeter.common.dto.TaskDTO;
import kz.bee.test.greeter.dao.TaskDao;
import kz.bee.test.greeter.dao.UserDao;
import kz.bee.test.greeter.entity.User;
import kz.bee.test.greeter.util.ConvertUtils;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class TaskListController implements Serializable {

    private static final String PAGE_SHOW = "pretty:show";
    private static final String PAGE_PRETTY = "pretty:";

    @Inject
    private GreeterController greeterController;
    @Inject
    private TaskDao taskDao;
    @Inject
    private UserDao userDao;


    private List<TaskDTO> taskList;

    public String show(String username) {
        String page = PAGE_PRETTY;
        if (username != null && !username.isEmpty()) {
            User user = userDao.getForUsername(username);
            if (user != null) {
                taskList = ConvertUtils.convert(taskDao.getTasksByUserId(user.getId()));
                page = PAGE_SHOW;
            }
        }
        return page;
    }

    public String edit(TaskDTO dto) {
        dto.setEditable(true);
        return null;
    }

    public String save(TaskDTO dto) {
        dto.setEditable(false);
        taskDao.updateTask(ConvertUtils.convert(dto,
                userDao.getUserForId(dto.getUserId())));
        return null;
    }

    public String done(TaskDTO dto) {
        dto.setEditable(false);
        dto.setStatus(TaskStatus.DONE.toString());
        taskDao.updateTask(ConvertUtils.convert(dto,
                userDao.getUserForId(dto.getUserId())));
        return null;
    }

    public String delete(TaskDTO dto) {
        dto.setEditable(false);
        taskList.remove(dto);
        taskDao.removeTask(dto.getId());
        return null;
    }

    public SelectItem[] getGenderValues() {
        SelectItem[] items = new SelectItem[TaskStatus.values().length];
        int i = 0;
        for (TaskStatus g : TaskStatus.values()) {
            items[i++] = new SelectItem(g, g.toString());
        }
        return items;
    }


    public List<TaskDTO> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<TaskDTO> taskList) {
        this.taskList = taskList;
    }
}
