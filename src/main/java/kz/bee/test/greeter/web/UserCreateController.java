package kz.bee.test.greeter.web;

import kz.bee.test.greeter.dao.UserDao;
import kz.bee.test.greeter.entity.User;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class UserCreateController implements Serializable {

    private static final String PAGE_INDEX = "pretty:index";
    @Inject
    private FacesContext facesContext;

    @Inject
    private UserDao userDao;

    @Named
    @Produces
    @RequestScoped
    private User newUser = new User();

    public String create() {
        try {
            userDao.createUser(newUser);
            String message = "A new user with id " + newUser.getId() + " has been created successfully";
            facesContext.addMessage(null, new FacesMessage(message));
        } catch (Exception e) {
           e.printStackTrace();
        }
        return PAGE_INDEX;
    }

}
