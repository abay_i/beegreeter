package kz.bee.test.greeter.web;

import kz.bee.test.greeter.common.TaskStatus;
import kz.bee.test.greeter.dao.TaskDao;
import kz.bee.test.greeter.dao.UserDao;
import kz.bee.test.greeter.entity.Task;
import kz.bee.test.greeter.entity.User;
import kz.bee.test.greeter.util.DateUtils;
import org.hibernate.validator.constraints.NotEmpty;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Named
@RequestScoped
public class TaskCreateController implements Serializable {

    private static final String PAGE_INDEX = "pretty:index";

    @Inject
    private FacesContext facesContext;

    @Inject
    private TaskDao taskDao;
    @Inject
    private UserDao userDao;

    @Size(min = 4, max = 16)
    private String username;

    @NotEmpty
    private String date;

    private UIComponent component;

    @Named
    @Produces
    @RequestScoped
    private Task newTask = new Task();


    public String create() {
        try {
            User user = userDao.getForUsername(username);
            if (user == null) {
                facesContext.addMessage(component.getClientId(), new FacesMessage("No such user exists!"));
                return "";
            }
            newTask.setUser(user);
            newTask.setDueDate(DateUtils.toDate(date));
            newTask.setStatus(TaskStatus.ASSIGNED.getValue());
            newTask.setActive(true);
            newTask.setCreateDate(new Date());
            taskDao.createTask(newTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PAGE_INDEX;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }
}
