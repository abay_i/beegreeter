package kz.bee.test.greeter.web;

import kz.bee.test.greeter.dao.UserDao;
import kz.bee.test.greeter.entity.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Named
@RequestScoped
public class GreeterController implements Serializable {

    @Inject
    private UserDao userDao;

    @NotNull
    @Size(min = 4, max = 16)
    private String username;

    private String greeting;

    public void greet() {
        User user = userDao.getForUsername(username);
        if (user != null) {
            greeting = "Hello, " + user.getFirstname() + " " + user.getLastname() + "!";
        } else {
            greeting = "No such user exists!";
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGreeting() {
        return greeting;
    }

}
